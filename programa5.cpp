#include<stdio.h>
#include<math.h>
#include<iostream>

using namespace std;


int Moda(int vector[],int arr);
float Media_aritmetica(int vector[],int arr);
float Desviacion_es(int vector[],int arr, float prom);

int main(){
	int num,EST,moda,notas;
	float media,desviacion;

	cout << "Ingrese la cantidad de alumnosmnos: ";
	cin>> notas;
	int I,alumnos[notas];

	for(I=0; I<notas;I++)
	{
		cout << "Ingrese la nota de examen N°" << I+1 << " :";
		cin >> num;
		alumnos[I]=num;
	}

	moda=Moda(alumnos,notas);
	cout <<"\nLa moda es: "<< moda;

	media=Media_aritmetica(alumnos,notas);
	cout <<"\nLa media aritmetica es: "<< media;

	desviacion = Desviacion_es(alumnos,notas,media);
	cout <<"\nLa desviacion estandar es: "<< desviacion;

	cout <<"\nLa varianza es: "<< desviacion*desviacion;
	cout <<"\n";
}

int Moda (int vector[],int arr)
{
	int I;
	float Valor = vector[0];
	float MOD = 0.0;
	for (I=1; I<arr; I++)
	    if(MOD<vector[I])
	    {
	    MOD = I;
	    Valor = vector[I];

	    }
	return (Valor);
}
float Media_aritmetica(int vector[],int arr)
{
	int I,promedio;
	float suma=0;
	for(I=0;I<arr;I++)
	{
		suma += vector[I]; // suma = suma +vector[I]
	}
	promedio = suma/arr;

	return promedio;
}
float Desviacion_es(int vector[],int arr,float prom)
{
	float SUM=0.0,S = 0.0;
	int I;
	for(I=0;I<arr;I++)
	{
		SUM = SUM + (vector[I]-prom)*(vector[I]-prom);
	}

	S = sqrt((SUM/(arr)));
	return S;
}
