#include <iostream>

using namespace std;

int main()
{
    int Numero_decimal; // Almacena el número decimal
    short Numero_binario[8];//Arreglo de 8 posiciones

    cout <<"Ingrese número entre 0 y 255: "; //Pide por pantalla el numero a convertir
    cin >> Numero_decimal;

    if (Numero_decimal>0){
        if (Numero_decimal<=255){

        for (int z = 0; z<8; z++) //ciclo for que divide por dos segun las posiciones del arreglo
        {
            Numero_binario[z] = Numero_decimal % 2; //Guarda un 0 o un 1 en el arreglo si es que es par o impar respectivamente
            Numero_decimal /= 2; //Divide por 2 la parte entera del decimal restante
        }
        cout<< "El numero equivalente en binario es: ";

        for (int k = 7; k>=0; k--)
        {
            cout<< Numero_binario[k]; //muestra el número según lo guardado en el arreglo
        }
        cout <<"\n";

        for (int i = 0; i<8; i++)
            {
            if(Numero_binario[i]==0){
                cout <<"El led N°"<< i << " esta APAGADO \n"; // Indica que el led del pin i esta apagado
            }
            else{
                cout <<"El led N°"<< i << " esta ENCENDIDO \n"; // Indica que el led del pin i esta encendido
            }
            }
        }
        else{
        cout <<"Ingrese un numero menor o igual a 255 \n"; //Condición cuando el numero ingresado es mayor a 255
        }
    }

    else{
    cout <<"Ingrese un numero entero positivo \n"; //Condición cuando el numero ingresado es menor a 0
    }
    return 0;
}

