#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
float S = 0.0; //Cantidad de Sonidos Emitidos
float FC = 0.0; //Temperatura en Celsius
float FA = 0.0; //Temperatura en Fahrenheit

cout <<"Ingrese el numero de sonidos emitidos: ";
cin >>S;

FA = S/4+40; //Temperatura en fahrenheit dependiendo el numero de sonidos

FC = (FA-32)*5/9; //Temperatura de fahrenheit a celsius

cout <<"\nLa temperatura en grados celsius es: "<< FC <<"°C\n";
return 0;
}

