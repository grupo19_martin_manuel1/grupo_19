#include <math.h>
#include <time.h>
#include <iostream>

using namespace std;

void Promedio(float); //declaracion de funciones
void Temp_Maxima(float, int);
void Temp_Minima(float, int);
//variables globales
float Prom = 0.0;  // Promedio de las temperaturas declarado en 0
float Maxima = -100.0; // Temperatura maxima declarada negativa para que el primer valor pueda modificar este valor
float Minima = 100.0; // Temperatura minima declarada muy alta para que el primer valor minimo modifique este valor

int Hora_temp_maxima;
int Hora_temp_minima;

int main()
{
float Temperatura;//variables locales
int i;
for (i=1; i<=24; i++)
{
cout<<"Ingresa la temperatura de las "<< i << ":00 horas: ";
cin >>Temperatura;
Promedio(Temperatura);
Temp_Maxima(Temperatura, i);
Temp_Minima(Temperatura, i);
}
cout<<"\n El promedio de la temperatura es: "<<(Prom/24)<< "°C";
cout<<"\n La Temperatura maxima es: " << Maxima <<"°C"<<"\t a las: " << Hora_temp_maxima << ":00 horas";
cout<<"\n La Temperatura minima es: " << Minima <<"°C"<<"\t a las: " << Hora_temp_minima << ":00 horas";
cout<<"\n";
}
void Promedio(float T)
{
Prom += T;
}
void Temp_Maxima(float T, int H)
{
if (Maxima<T)
{
Maxima = T;
Hora_temp_maxima = H;
}
}
void Temp_Minima(float T, int H)
{
if (Minima>T)
{
Minima = T;
Hora_temp_minima = H;
}
}


